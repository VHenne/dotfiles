runtime! archlinux.vim
" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible              " be iMproved
filetype off                  " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required!
Plugin 'gmarik/vundle'

Plugin 'Gundo'
Plugin 'tpope/vim-abolish'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
"Plugin 'vdecine/toggleheader'
Plugin 'Valloric/YouCompleteMe'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-fugitive'
"Plugin 'myusuf3/numbers.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'MaxSt/FlatColor'
Plugin 'octol/vim-cpp-enhanced-highlight'

Plugin 'Lokaltog/powerline-fonts'
Plugin 'bling/vim-airline'
Plugin 'airblade/vim-gitgutter'
Plugin 'flazz/vim-colorschemes'
Plugin 'jnurmine/Zenburn'
Plugin 'skammer/vim-css-color'
Plugin 'tpope/vim-surround'
Plugin 'dantler/vim-alternate'
Plugin 'godlygeek/tabular'
Plugin 'majutsushi/tagbar'

Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'bufexplorer.zip'
Plugin 'kien/ctrlp.vim'
Plugin 'DoxygenToolkit.vim'

Plugin 'honza/vim-snippets'
Plugin 'SirVer/ultisnips'
Plugin 'Raimondi/delimitMate'


" Attempt to determine the type of a file based on its name and possibly its
" contents.  Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype plugin indent on     " required!

" To ignore plugin indent changes, instead use:
" filetype plugin on
"
" Brief help
" :PluginList          - list configured plugins
" :PluginInstall(!)    - install (update) plugins
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused plugins
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Plugin commands are not allowed.
" Put your stuff after this line

" Enable syntax highlighting
syntax enable

"------------------------------------------------------------
" Must have options {{{1
"
" These are highly recommended options.

" One of the most important options to activate. Allows you to switch from an
" unsaved buffer without saving it first. Also allows you to keep an undo
" history for multiple files. Vim will complain if you try to quit without
" saving, and swap files will keep you safe if your computer crashes.
set hidden

" Better command-line completion
set wildmenu"

" Ignore binart/build files when completing/searching
set wildignore+=*/build/*,*.so,*.swp,*.zip,*.o,.git/*,.hg/*,.svn/*,*.db

" Don't redraw while executing macros (good performance config)
set lazyredraw

" Do not try to display latex math as unicode
let g:tex_conceal = ""

" Show partial commands in the last line of the screen
set showcmd

" Set to auto read when a file is changed from the outside
set autoread

" Remove menu bars in gvim
if has('gui_running')
  set guioptions = "agirLt"
"  set guioptions -= 'T' " Get rid of toolbar
"  set guioptions -= 'm' " Get rid of menu
endif

" Modelines have historically been a source of security vulnerabilities.  As
" such, it may be a good idea to disable them and use the securemodelines
" script, <http://www.vim.org/scripts/script.php?script_id=1876>.
 set nomodeline

" auto-reload the vimrc
augroup reload_vimrc " {
  autocmd!
  autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END "}

"" recognize Alt key in gnome-terminal
"let c='a'
"while c <= 'z'
"  exec "set <A-".c.">=\e".c
"  exec "imap \e".c." <A-".c.">"
"  let c = nr2char(1+char2nr(c))
"endw

" set timeout ttimeoutlen=50

" show prettier line wraps
set showbreak=↪

" remove menu-shortcuts using alt key
set winaltkeys=no

" show and remove trailing whitespaces
match ErrorMsg '\s\+$'

function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction

function! ConvertToUnixFileFormat()
  %s/\r//g
endfunction

autocmd FileWritePre   * :call TrimWhiteSpace()
autocmd FileWritePre   * :call ConvertToUnixFileFormat()
autocmd FileAppendPre  * :call TrimWhiteSpace()
autocmd FilterWritePre * :call TrimWhiteSpace()
autocmd BufWritePre    * :call TrimWhiteSpace()

"------------------------------------------------------------
" Usability options {{{1
"
" These are options that users frequently set in their .vimrc. Some of them
" change Vim's behaviour in ways which deviate from the true Vi way, but
" which are considered to add usability. Which, if any, of these options to
" use is very much a personal preference, but they are harmless.

" Use case insensitive search, except when using capital letters
set incsearch
set ignorecase
set smartcase

" fix the alt-modifier
let c='a'
while c <= 'z'
  exec "set <A-".c.">=\e".c
  exec "imap \e".c." <A-".c.">"
  let c = nr2char(1+char2nr(c))
endw

set ttimeout ttimeoutlen=50


" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
set hlsearch

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" set smart indent
set smartindent

" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" cursor
hi CursorLine term=bold,underline cterm=bold,underline ctermbg=NONE
hi CursorColumn term=bold cterm=bold ctermbg=NONE

" Change Color when entering Insert Mode
autocmd InsertEnter * set cursorline! cursorcolumn!


" Revert Color to default when leaving Insert Mode
autocmd InsertLeave * set cursorline! cursorcolumn!

" Always display the status line, even if only one window is displayed
set laststatus=2

" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm

" Persistent undo
try
  if MySys() == "windows"
    set undodir=C:\Windows\Temp
  else
    set undodir=~/.vim/undo
  endif
  set undofile
catch
endtry

" Use visual bell instead of beeping when doing something wrong
set noerrorbells visualbell
autocmd GUIEnter * set visualbell t_vb=

" And reset the terminal code for the visual bell.  If visualbell is set, and
" this line is also included, vim will neither flash nor beep.  If visualbell
" is unset, this does nothing.
set vb
set t_vb=

" Enable use of the mouse for all modes
" Do not enable mouse for all modes because this leads to problems with copy
" and paste.
set mouse=a

" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=3

" Display line numbers on the left
set number
" Up to 99999 lines
set numberwidth=5

" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200

" Use <F2> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F2>

" Set my favorite colorscheme
" Also enable this colorscheme in the shell
set t_Co=256
"colorscheme desert
if has("gui_running")
  set guifont=DejaVuSansMono\ for\ Powerline
"   colorscheme wombat
endif
  colorscheme molokai "flatcolor

"let g:molokai_original=1

" Hex
noremap <F8> :call HexMe()<CR>

let $in_hex=0
function HexMe()
  set binary
  set noeol
  if $in_hex>0
    :%!xxd -r
    let $in_hex=0
  else
    :%!xxd
    let $in_hex=1
  endif
endfunction

"------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.

" Indentation settings for using 2 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set tabstop=8
set expandtab
set shiftwidth=2 " for indentation
set softtabstop=2

" Show whitespaces
syn match TAB "\t"
hi link TAB Error
"
" we do what to show tabs, to ensure we get them out of my files
set list
"set linebreak
"set nolist

" show tabs and trailing whitespace
set listchars=tab:└─,trail:-,extends:>,precedes:<,nbsp:&

" how many tenths of a second to blink matching brackets
set matchtime=5

" Keep 5 lines (top/bottom) for scope
set scrolloff=10

" show matching brackets
set showmatch

"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <a-l> :noh<return><a-l>

" remove Ex mode
nnoremap Q <nop>


" Florian's config file

" Save swapfiles in tmp instead of working dir
set directory=/tmp

" Do not use swap files
set noswapfile

" always switch to the current directory of the file you are
set autochdir

" custom statusline
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
set laststatus=2

" improve the way autocomplete works
set completeopt=menu,longest
" prevent vim from searching in all included files
set complete-=i

" do not wrap line
" hightlight current column
" set cursorcolumn

" highlight current line
"set cursorline

" do not wrap lines at the end of the screen
set nowrap

" normal copy/paste
map <C-c> "+y
vmap <C-c> "+y

map <C-x> "+d
vmap <C-x> "+d

map <C-v> "+gP
vmap <C-v> "+gP
imap <C-v> <ESC>"+gpa

" I always like to save with ctrl-s
map <C-S> :w<CR>
" map! <C-S> <ESC>:w<CR>li
vmap <C-S> <ESC>:w<CR>v
imap <C-S> <ESC>:w<CR>a

" Prevent replace mode
imap <Insert> <Nop>

" Navigate in word wrapped text with the usual navigation keys and with the up
" and down keys
nnoremap k gk
nnoremap j gj
nmap <Up> gk
nmap <Down> gj

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Shortcuts for moving between tabs (taken from www.swaroopch.com)
" Alt-j to move to the tab on the left
noremap <a-j> gT
" Alt-k to move to the tab on the right
noremap <a-k> gt

" I always write W instead of w and Q instead of Q
command WQ wq
command W w
command Q q
set colorcolumn=100

" Plugin configuration
" Gundo
nnoremap <F5> :GundoToggle<CR>
inoremap <F5> <ESC> :GundoToggle<CR>

" abolish
abbr :S :Subvert
" crs (snake_case), crm (MixedCase), crc (camelCase)

" NerdCommenter
map <M-c> <plug>NERDCommenterToggle
imap <M-c> <plug>NERDCommenterToggle
vmap <M-c> <plug>NERDCommenterToggle

" alternate
map <C-a> :A<CR>
imap <C-a> :A<CR>
vmap <C-a> :A<CR>

map <C-y> :AV<CR>
imap <C-y> :AV<CR>
vmap <C-y> :AV<CR>

let g:alternateSearchPath = 'sfr:../source,sfr:../src,sfr:../include,sfr../inc,reg:/include/src/g/,reg:/inc/src/g/,reg:/src/include/g/,reg:/src/inc/g/,reg:/source/include/g/'

" UltiSnips
let g:UltiSnipsExpandTrigger="<c-j>"
"let g:UltiSnipsListSnippets="<c-s-tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" CtrlP (similar to FuzzyFinder)
map <Leader>ff :CtrlP<CR>
map <Leader>fb :CtrlPBuffer<CR>

" NERDTree
nnoremap <F3> :NERDTreeToggle<CR>

" YCM
let g:ycm_allow_changing_updatetime=1
let g:ycm_key_invoke_completion = '<C-Space>'
let g:ycm_confirm_extra_conf = 0
let g:ycm_filepath_completion_use_working_dir = 0
" YCM configs for automatic config generation
let g:ycm_path_root_code_folder = "/home/kurayami/code/src"

" Syntastic
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_enable_balloons = 1
let g:syntastic_aggregate_errors = 1
let g:syntastic_enable_signs = 1

" EasyMotion
let g:EasyMotion_leader_key = "\\\\"
hi link EasyMotionTarget ErrorMsg
hi link EasyMotionShade Comment

" airline
let g:airline_powerline_fonts=1
let g:airline_theme="simple"
let g:airline#extensions#tabline#enabled = 1

" Flatcolor
let g:flatcolor_cursorlinebold = 1
let g:flatcolor_termcolors = 1

" Define mapleader \ at the end of the file for all the plugins that use a leader key
let mapleader = "\\"
